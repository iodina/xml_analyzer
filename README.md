# XML Analyzer #

XML Analyzer is program analyzing HTML and finds a specific element, even after changes, using a set of extracted attributes


### How do I get set up? ###

* How to run script
---
    cd xml_analyzer/
    pip install -r requirements.txt
    python main.py <input_origin_file_path> <input_other_sample_file_path>
    OR
    python main.py <input_origin_file_path> <input_other_sample_file_path> <element_id>
---

* How to run tests
---
    pip install -r tests/test_requirements.txt
    pytest -v test  --junitxml=test-reports/report.xml
---
