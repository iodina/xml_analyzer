import sys
import os

from src.finder import Finder


def find_path(origin_path: str, sample_path: str, element_id: str = None):
    return Finder(path_origin=origin_path, path_sample=sample_path, element_id=element_id).find()


def main(args=None):
    args = args or sys.argv[1:]

    # Validate number of input parameters
    if len(args) not in {2, 3}:
        raise AttributeError
    else:
        # Validate files actually exist
        if not os.path.exists(args[0]):
            raise FileNotFoundError(f"No such file {args[0]} on directory")
        if not os.path.exists(args[1]):
            raise FileNotFoundError(f"No such file {args[1]} on directory")

    try:
        path_to_element = find_path(*args)
    except NotImplementedError as e:
        sys.stdout.write(f"Sorry, the algorithm failed: {e}")
    else:
        sys.stdout.write(f"Path has been successfully found: {path_to_element}")


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        sys.stderr.write(f"Something went terribly wrong: {e}")
