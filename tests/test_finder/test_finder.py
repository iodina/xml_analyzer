import os

import pytest

from src.constants import SAMPLE_DATA_DIR
from src.finder import Finder


expected_1 = 'html>body>div>div>div[3]>div[1]>div>div[2]>a[2]'
expected_2 = 'html>body>div>div>div[3]>div[1]>div>div[2]>div>a'
expected_3 = 'html>body>div>div>div[3]>div[1]>div>div[3]>a'
expected_4 = 'html>body>div>div>div[3]>div[1]>div>div[3]>a'


def _get_path(path):
    file_path = "/".join([SAMPLE_DATA_DIR, path])
    return os.path.join("/".join(os.path.dirname(__file__).split("/")[:-1]), file_path)


origin_path = _get_path("sample-0-origin.html")

sample1 = _get_path("sample-1-evil-gemini.html")
sample2 = _get_path("sample-2-container-and-clone.html")
sample3 = _get_path("sample-3-the-escape.html")
sample4 = _get_path("sample-4-the-mash.html")
sample5 = _get_path("sample-5-failure.html")


@pytest.mark.parametrize("sample_path, expected", [
    (sample1, expected_1),
    (sample2, expected_2),
    (sample3, expected_3),
    (sample4, expected_4),

])
def test_finder_success(sample_path, expected):
    result = Finder(path_origin=origin_path, path_sample=sample_path).find()
    assert expected == result


@pytest.mark.parametrize("sample_path, expected", [
    (sample5, NotImplementedError),
])
def test_finder_failure(sample_path, expected):
    with pytest.raises(expected):
        Finder(path_origin=origin_path, path_sample=sample_path).find()
