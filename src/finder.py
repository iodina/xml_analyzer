from collections import Counter

from bs4 import BeautifulSoup

from src.constants import DEFAULT_ELEMENT_ID
from src.utils import prepare_path


class Finder:

    def __init__(self, path_origin, path_sample, element_id=None):
        self.path_origin = path_origin
        self.path_sample = path_sample
        self.element_id = element_id or DEFAULT_ELEMENT_ID

    @property
    def origin(self):
        with open(self.path_origin) as f:
            data = f.read()
        return BeautifulSoup(data, features="html.parser")

    @property
    def sample(self):
        with open(self.path_sample) as f:
            data = f.read()
        return BeautifulSoup(data, features="html.parser")

    def find(self):
        origin_element = self._find_origin_element_by_id(self.element_id)

        target_elements = self._find_elements_by_element_attrs(origin_element)
        target_elements.extend(self._find_elements_by_text(origin_element))

        counter = self._count_target_elements(elements=target_elements)

        is_target_found = counter.most_common()[0] \
            if counter.most_common()[0][1] > counter.most_common()[1][1] \
            else None
        if is_target_found:
            target_element = counter.most_common()[0][0]
            return prepare_path(target_element)
        else:
            raise NotImplementedError(f"Search Engine is not smart enough to find the element {self.element_id}")

    def _find_elements_by_text(self, element):
        return self.sample.findAll(name=element.name, attrs={}, text=element.text)

    def _find_elements_by_attrs(self, attrs):
        return self.sample.findAll(attrs=attrs)

    def _find_elements_by_element_attrs(self, element):
        elements = []
        for attr_name, attr_value in element.attrs.items():
            elements.extend(self._find_elements_by_attrs({attr_name: attr_value}))
        return elements

    @staticmethod
    def _count_target_elements(elements):
        counter = Counter()
        for e in elements:
            counter[e] += 1
        return counter

    def _find_origin_element_by_id(self, element_id):
        return self.origin.find(id=element_id)
